reset
set title 'Aufgabe 2 - Simulation 1000 Photonen, 0.10m Radius';
set xlabel 'Entweichzeit [s]';
set ylabel 'Anzahl der Photonen';
set grid;
set terminal pdf;
set output 'pdf/verteilung1.pdf';
plot 'data/verteilung1.data' title 'Datenpunkte' with boxes;