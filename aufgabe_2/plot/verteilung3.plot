reset
set title 'Aufgabe 2 - Simulation 1000 Photonen, 0.70m Radius';
set xlabel 'Entweichzeit [s]';
set ylabel 'Anzahl der Photonen';
set grid;
set terminal pdf;
set output 'pdf/verteilung3.pdf';
plot 'data/verteilung3.data' title 'Datenpunkte' with boxes;