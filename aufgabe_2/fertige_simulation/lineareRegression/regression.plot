reset
set title 'Aufgabe 2 - Lineare Regression
set xlabel 'log(Sonnenradius [m])';
set ylabel 'log(Entweichzeit [s])';
f(x) = 1.989536*x+-4.981900;
set grid;
set terminal pdf;
set output 'lineareRegression/regression.pdf';
plot 'lineareRegression/regression.data' title 'Datenpunkte',f(x);