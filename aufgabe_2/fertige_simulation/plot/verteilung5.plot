reset
set title 'Aufgabe 2 - Simulation 1000 Photonen, 1.30m Radius';
set xlabel 'Entweichzeit [s]';
set ylabel 'Anzahl der Photonen';
set grid;
set terminal pdf;
set output 'pdf/verteilung5.pdf';
plot 'data/verteilung5.data' title 'Datenpunkte' with boxes;