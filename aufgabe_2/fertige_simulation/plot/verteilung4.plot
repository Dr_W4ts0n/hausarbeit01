reset
set title 'Aufgabe 2 - Simulation 1000 Photonen, 1.00m Radius';
set xlabel 'Entweichzeit [s]';
set ylabel 'Anzahl der Photonen';
set grid;
set terminal pdf;
set output 'pdf/verteilung4.pdf';
plot 'data/verteilung4.data' title 'Datenpunkte' with boxes;