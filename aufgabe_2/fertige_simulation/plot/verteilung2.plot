reset
set title 'Aufgabe 2 - Simulation 1000 Photonen, 0.40m Radius';
set xlabel 'Entweichzeit [s]';
set ylabel 'Anzahl der Photonen';
set grid;
set terminal pdf;
set output 'pdf/verteilung2.pdf';
plot 'data/verteilung2.data' title 'Datenpunkte' with boxes;