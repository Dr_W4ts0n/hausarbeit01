#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*
 * Deklaration aller Funktionen, aus Übersichts- und Funktionsgründen
 */
double mittelwertEinlesen(int);
double radiusEinlesen(int);
void lineareRegression(int);
void datenDatei(int);
void plotDatei(double, double);

int main(){
	lineareRegression(5);//anzahl an Simulationen übergeben
	system("gnuplot -persist lineareRegression/regression.plot");
	return 0;
}

/*
 * Liest den Entweichzeit-Mittelwert aus der Simulations-Datendatei
 * Parameter: Zahl der jeweiligen Simulation als int
 * Return: Mittelwert in Sekunden als double 
 */
double mittelwertEinlesen(int n){
	n++; //Anzahl soll bei 1 anfangen, nicht bei 0
	char dataFileName[30];
	char zahl[5];
	
	sprintf(zahl, "%d", n); //dynamischer Dateiname, um die richtige Datei zu öffnen
	strcpy(dataFileName, "data/verteilung");
	strcat(dataFileName, zahl);
	strcat(dataFileName, ".data");
	
	FILE *fp;
	char temp[100]; //hier werden unsere ausgelesenen Werte temporär gespeichert
	
	int readbyte; //Hilfsvariable um das Ende der Datei zu finden
	int zeilen = -1; //Eine Zeile weniger, da die erste nicht zählt
	fp = fopen(dataFileName, "r");
	if(fp == NULL){
		printf("Datei konnte nicht geoffnet werden.\n");
	}else{
		fscanf(fp, "%s", temp); //erste Zeile überspringen, da hier der Sonnenradius notiert ist
		 while ((readbyte = fgetc(fp)) != EOF){ //Haben wir das Ende erreicht?
			if (readbyte == '\n'){
				zeilen++; //Wieviele Zeilen haben wir? Brauchen wir, um zu wissen wieviele Werte wir erhalten
			}
		}
		fclose(fp);
	}
	double x_achse[zeilen]; //hier Werden unsere Entweichzeiten gespeichert EINHEIT: [s]
	int y_achse[zeilen]; //hier werden die Häufigkeiten gespeichert
	
	fp = fopen(dataFileName, "r");
	if(fp == NULL){
		printf("Datei konnte nicht geoffnet werden.\n");
	}else{
		fscanf(fp, "%s", temp); //erste Zeile überspringen, da hier der Sonnenradius notiert ist
		for(int i=0, j=0, k=0; i<zeilen*2;i++){
			if(i%2 == 0){
				fscanf(fp, "%s", temp);
				x_achse[j] = atof(temp); //wir brauchen diese Werte als Double
				j++;
			}else{
				fscanf(fp, "%s", temp);
				y_achse[k] = atoi(temp); //diese Werte brauchen wir als int
				k++;
			}
		}
		
		fclose(fp);
	}
	
	double mittelwert = 0; //Mittelwert der Entweichzeit EINHEIT: [s]
	int gesamtAnzahl = 0; //Gesamtanzahl an entwichenen Photonen
	for(int i=0; i<zeilen;i++){
		gesamtAnzahl += y_achse[i];
		mittelwert += y_achse[i]*x_achse[i];
	}
	mittelwert /= gesamtAnzahl; //berechnung des Mittelwerts
	return mittelwert; //Rückgabe des Mittelwerts einer einzige Datendatei
}


/*
 * Liest den Sonnenradius in Meter aus der Simulations-Datendatei
 * Parameter: Zahl der jeweiligen Simulation als int
 * Return: Sonnenradius in Sekunden als double 
 */
double radiusEinlesen(int n){
	n++; //Anzahl soll bei 1 anfangen, nicht bei 0
	char dataFileName[30];
	char zahl[5];
	
	sprintf(zahl, "%d", n); //dynamischer Dateiname, um die richtige Datei zu öffnen
	strcpy(dataFileName, "data/verteilung");
	strcat(dataFileName, zahl);
	strcat(dataFileName, ".data");
	
	char radiusEinlesen[10]; //hier wird zunächst die erste Zeile der Datei eingespeichert in der form #[radius]
	char *pointerRadius = radiusEinlesen; //dieser Zeiger wird auf das zweite Element von unserem C-String zeigen,
										  //um das "#" wegzubekommen
	double radius; //hier wird dann der gecastete Wert des Radius gespeichert EINHEIT: [m]
	FILE *fp;
	
	fp = fopen(dataFileName, "r");
	if(fp == NULL){
		printf("Datei konnte nicht geoffnet werden.\n");
	}else{
		fscanf(fp, "%s", radiusEinlesen);

		pointerRadius++; //zweige auf das zweite Element von radiusEinlesen[]
		radius = atof(pointerRadius); //wir brauchen eine double als Wert
		fclose(fp);
	}
	
	return radius;
}

/*
 * Führt die lineare Regression durch
 * Parameter: Zahl der jeweiligen Simulation als int
 * Return: kein Return
 */
void lineareRegression(int n){
	double sonneRadius = 500000000;	//wahrer Radius der Sonne EINHEIT: [m]
	double ergebnis; //Entweichzeit für den wahren sonneRadius EINHEIT: [y] (Jahre)
	double daten[2][n]; //die zu plottende Daten: daten[0][n] -> x-Achse EINHEIT: [log(m)]
						//daten[1][n] -> y-Achse EINHEIT: [log(s)]
	double xQuer; //selbsterklärend
	double yQuer;
	double xyQuer;
	double xQuadQuer;
	double b; //y-Achsenabschnitt EINHEIT: [log(s)]
	double a; //Steigung EINHEIT: [log(s)/log(m)]
	
	for(int i=0;i<n;i++){
		daten[0][i] = log10(radiusEinlesen(i)); //Logarithmisiere die Daten und speichere sie
		daten[1][i] = log10(mittelwertEinlesen(i));
		printf("radius: %f\t", daten[0][i]);
		printf("t_esc: %.30f\n", daten[1][i]);
	}
	/*Berechnung der notwendigen Mittelwerte für die lineare Regression*/
	for(int i=0;i<n;i++){
		xQuer += daten[0][i];
		yQuer += daten[1][i];
		xyQuer += daten[0][i]*daten[1][i];
		xQuadQuer += pow(daten[0][i],2);
	}
	xQuer /= n;
	yQuer /= n;
	xyQuer /= n;
	xQuadQuer /= n;
	/*Berechnung der notwendigen Mittelwerte für die lineare Regression*/
	
	a = (xyQuer - yQuer*xQuer)/(xQuadQuer - pow(xQuer,2));
	b = yQuer - a*xQuer;
	
	printf("Steigung: %f\t", a);
	printf("y-Achsenabschnitt: %f\n", b);
	
	datenDatei(n);
	plotDatei(a,b);
	
	//////NUN WIRD HIER t_esc für sonneRadius berechnet
	sonneRadius = log10(sonneRadius); //EINHEIT: [log(m)]
	
	ergebnis = a*sonneRadius+b;
	ergebnis = pow(10, ergebnis);
	ergebnis /= 31536000; //umrechnung von [s] in [y]
	printf("t_esc = %.2fy\n", ergebnis);
}

/*
 * Erstellt die Datendatei für GNUPLOT
 * Parameter: Zahl der aktuellen Simulation als int
 * Return: kein Return
 */
void datenDatei(int n){
	double daten[2][n];
	for(int i=0;i<n;i++){//Hier lesen wir wieder die ganzen Daten ein
		daten[0][i] = log10(radiusEinlesen(i));
		daten[1][i] = log10(mittelwertEinlesen(i));
	}
	
	FILE *fp_daten;
	fp_daten = fopen("lineareRegression/regression.data", "w");
	
	if(fp_daten == NULL){
		printf("Datei konnte nicht geoeffnet werden.\n");
	}else{
		for(int i=0; i<n; i++){
			fprintf(fp_daten, "%.30f\t", daten[0][i]);// Speichere die Daten in die Datendatei
			fprintf(fp_daten, "%.30f\n", daten[1][i]);
		}
		
		fclose(fp_daten);
	}
}

/*
 * Erstellt die Plotdatei für GNUPLOT
 * Parameter: Steigung als double, y-Achsenabschnitt als double
 * Return: kein Return
 */
void plotDatei(double a, double b){
	
	FILE *fp_plot;
	
	fp_plot = fopen("lineareRegression/regression.plot", "w");
	
	if(fp_plot == NULL){
		printf("Datei konnte nicht geoeffnet werden.\n");
	}else{
		fprintf(fp_plot, "reset\nset title 'Aufgabe 2 - Lineare Regression\n");
		fprintf(fp_plot, "set xlabel 'log(Sonnenradius [m])';\nset ylabel 'log(Entweichzeit [s])';\n");
		fprintf(fp_plot, "f(x) = %f*x+%f;\n", a, b); //Erstelle unsere berechnete Funktion
		fprintf(fp_plot, "set grid;\nset terminal pdf;\nset output 'lineareRegression/regression.pdf';\n");
		fprintf(fp_plot, "plot 'lineareRegression/regression.data' title 'Datenpunkte',f(x);"); //Plotte die Funktion und die Datenpunkte
		fclose(fp_plot);
	}
}
