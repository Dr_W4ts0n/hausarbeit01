#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/*
 * Hier werden zunächst unsere Konstanten als Makros definiert.
 */
#define sigma  6.65*pow(10,-29) //EINHEIT: [m^2] Thomsonstreueung
#define m_h  1.008*(1.660539040*pow(10,-27)) //EINHEIT: [kg] Masse Wasserstoff
#define c 	299792458 //EINHEIT: [m/s] Lichtgeschwindigkeit
#define M_PI 3.14159265

/*
 * Deklaration aller Funktionen, aus Übersichts- und Funktionsgründen
 */
double getWeglaenge(double*, double);
double *getRichtungsvektor();
void aktualisiereOrt(double*, double);
double photonDurchgang(int, double);
void erstelleHistogramm(int, int, double);
double getBinBreite(double);
void datenDatei(int, int, double*, int*, double);
void plotDatei(int, int, double);



int main(){
	srand(time(NULL));
	printf("Ich rechne (5 Simulationen), bitte warte...\n");
	
	erstelleHistogramm(1,1000,0.1); //übergebe Zahl der aktuellen Simulation, Photonenanzhal, Radius der Sonne in [m]
	erstelleHistogramm(2,1000,0.4);
	erstelleHistogramm(3,1000,0.7);
	erstelleHistogramm(4,1000,1.0);
	erstelleHistogramm(5,1000,1.3);
	system("gnuplot -persist plot/verteilung1.plot");
	system("gnuplot -persist plot/verteilung2.plot");
	system("gnuplot -persist plot/verteilung3.plot");
	system("gnuplot -persist plot/verteilung4.plot");
	system("gnuplot -persist plot/verteilung5.plot");
	
	printf("\nFertig!\n");
    return 0;
}
/*
 * Diese Funktion berechnet die Weglänge.
 * Parameter: Ortsvektor als Zeiger zum double Array, Sonnenradius in Meter als double-Wert
 * Return: Weglänge als double
 */
double getWeglaenge(double *ortsvektor, double sonneRadius){
	double r; //EINHEIT: [m] aktueller Abstand zum Mittelpunkt [0,0,0]
	double dichte; //EINHEIT: [kg/m^3] Dichte an unserem Punkt
	double weglaenge;//EINHEIT: [m] freie Weglänge
	
	r = sqrt(pow(*ortsvektor, 2) + pow(*(ortsvektor+1), 2) + pow(*(ortsvektor+2), 2)); //Abstand EINHEIT: [m]
	dichte = -(3*pow(10,4))*((5*r)/sonneRadius) + (1.5*pow(10,5)); //Dichte der Sonne an r EINHEIT: [kg/m^3]
	weglaenge = m_h/(sigma*dichte); //Weglänge EINHEIT: [m]
	
	return weglaenge;
}

/*
 * Erzeugt einen kartesischen Vektor mit zufälliger Richtung und Laenge 1
 * Parameter: keine
 * Return: Pointer der x-Koordinate, des Arrays in dem der Vektor abgespeichert wird
 * */
 
double *getRichtungsvektor(){
    double phi = ((double)rand()/RAND_MAX)*2*M_PI;
    double theta = ((double)rand()/RAND_MAX)*M_PI;
    static double richtungsvektor[3];                   //3-Zellen-Array entspricht dem Vektor
    
    richtungsvektor[0] = sin(theta)*cos(phi);             //x-Koordinate
    richtungsvektor[1] = sin(theta)*sin(phi);             //y-Koordinate
    richtungsvektor[2] = cos(theta);                      //z-Koordinate
    
    return richtungsvektor;
}
/*
 * Aktualisiert den Ortsvektor
 * Parameter: Ortsvektor als Zeiger zum double Array, Weglange in Meter als double
 * Return: kein Return
 */
void aktualisiereOrt(double *ortsvektor, double weglaenge){
    double *richtungsvektor;
    richtungsvektor = getRichtungsvektor();
    
    *ortsvektor = *ortsvektor+(*richtungsvektor)*weglaenge; // neue x-Koordinate EINHEIT: [m]
    *(ortsvektor+1) = *(ortsvektor+1)+(*(richtungsvektor+1))*weglaenge; //neue y-Koordinate EINHEIT: [m]
    *(ortsvektor+2) = *(ortsvektor+2)+(*(richtungsvektor+2))*weglaenge; //neue z-Koordinate EINHEIT: [m]
}
/*
 * Führt einen Photondurchgang
 * Parameter: Zahl des aktuellen Photons als int, der Sonnenradius in Meter als double
 * Return: Entweichzeit des Photons in Sekunden als double
 */
double photonDurchgang(int n_photon, double sonneRadius){
	double ort[3]={0.0,0.0,0.0}; //EINHEIT: {[m],[m],[m]}
	double r = 0; //Abstand zum Mittelpunkt EINHEIT: [m]
	double t_esc = 0; //Entweichzeit EINHEIT: [s]
	double weglaenge = 0; //Weglänge EINHEIT: [m]
	
	while(r<sonneRadius){ //Führe die Simulation durch, solange unser Photon sich in der Sonne befindet
		weglaenge = getWeglaenge(ort, sonneRadius); //Weglänge aktualisieren in Abhängikeit des Ortes
		aktualisiereOrt(ort, weglaenge);
		t_esc += weglaenge; //zunächst ist t_esc die Gesamtweglänge, hat also EINHEIT: [m]
							//wird aber gleich mit der Lichtgeschwindigkeit in [s] umgerechnet
		r = sqrt(pow(ort[0], 2) + pow(ort[1], 2) + pow(ort[2], 2));
	}
	
	t_esc /= c; //Hier geschieht die Umrechnung um die Entweichzeit zu erhalten EINHEIT: [s]
	return t_esc;
}
/*
 * Erstellt das Histogramm einer einzelnen Simulation
 * Dies ist die Funktion, die in der Main-Funktion aufgerufen wird, um die Simulation durchzuführen
 * Parameter: Zahl der aktuellen Simulation als int, Anzahl der zu entkommenden Photonen als int, Sonnenradius in Meter als double
 * Return: kein Return 
 */
void erstelleHistogramm(int n, int durchlaeufe, double sonneRadius){
	double photonen[durchlaeufe];	//Array aller Photonen
	double min = photonDurchgang(0, sonneRadius); //wir suchen den kleinsten Wert für t_esc,
												  //diesen setzen wir zunächst gleich dem ersten Wert EINHEIT: [s]
	double max = photonDurchgang(0, sonneRadius); //gleiches Prinzip wie beim Minimum EINHEIT: [s]
	double bin;	//hier wird die Binbreite gespeichert EINHEIT:  [s]
	
	for(int i=0;i<durchlaeufe;i++){ //hier führen wir die Simulationen durch. Also alle Photonendurchgänge
		photonen[i] = photonDurchgang(i, sonneRadius);
		if(photonen[i] < min){
			min = photonen[i]; //falls wir einen kleineren Wert für t_esc finden, aktualisieren wir diesen
		}
		if(photonen[i] > max){
			max = photonen[i]; //gleiches Prinzip, wie beim Minimum
		}
		//printf("%.30f\n", photonen[i]);
	}
	//printf("\n");
	
	
	bin = getBinBreite(min); //wir passen die Binbreite an den kleinsten Wert an, um eine gute Auflösung zu erhalten
	int anzahlBin = 0;
	max += bin/2; //damit der letzte y-Wert auch reingeschrieben wird
	/*for(double d=min; d<=max; d += bin){
		anzahlBin++;
	}*/
	
	double d=min;
	while(d<=max){
		anzahlBin++;
		d += bin;
	}

	
	double x_achse[anzahlBin]; //t in s
	int y_achse[anzahlBin]; //Anzahl an Photonen
	
	for(int i=0; i<anzahlBin;i++){
		x_achse[i] = min+i*bin; //wir gehen auf der x-Achse in Schritten der Binbreite vor, von min bis max
		y_achse[i] = 0; //setzen gleichzeit zur sicherheit alle y-Werte = 0

	}
	
	for(int i=0; i<durchlaeufe; i++){
		for(int j=0; j<anzahlBin; j++){
			if(photonen[i] >= x_achse[j]-(bin/2) && photonen[i] <= x_achse[j]+(bin/2)){
				y_achse[j]++; //wenn t_esc des Photons im entsprechend Intervall, erhöhen wir unseren y-Wert
			}
		}
	}
	
	
	datenDatei(n, anzahlBin, x_achse, y_achse, sonneRadius); //schreiben die Daten in die Datendatei
	plotDatei(n, durchlaeufe, sonneRadius);	//schreiben unsere Plotdatei
	
	printf("Simulation %d ist fertig!...\n", n);
}

/*
 * Gibt uns die Breite eines einzelnen Bins für das Histogramm
 * Parameter: die kleinste Entweichzeit der aktuellen Simulation als double
 * Return: Binbreite als double
 */
double getBinBreite(double wert){
	double zahl = wert;
	int exponent = 0;
	int stelle = (int)zahl;
	if(zahl < 1){ //falls unser Minimum < 1, wollen wir einen negativen Exponenten
				  //dies ist bei unseren Simulationen immer der fall. Aus Vollständigkeitsgründen
				  //wird die Unterscheidung hier implementiert
		while(stelle == 0){
			zahl *= 10;
			stelle = (int)zahl;
			exponent--;
		}
	}else{
		while(stelle >= 10){
			zahl /= 10;
			stelle = (int)zahl;
			exponent++;
		}
	}
	
	return 1*pow(10, exponent); //Binbreite ist in der Größenordnung des Minimums
}

/*
 * Erstellt die Datendatei für GNUPLOT
 * Parameter: Zahl der aktuellen Simulation als int, Anzahl der Bins als int,
 * 	Zeiger zum x-Daten (Anzahl der Photonen) Array als double,
 * 	Zeiger zum y-Daten (Entweichzeit in Sekunden) Array als double,
 * 	Sonnenradius in Meter als double
 * Return: kein Return
 */
void datenDatei(int n, int anzahlBin, double *x_achse, int *y_achse, double sonneRadius){
	char dataFileName[30];
	char zahl[5];
	
	sprintf(zahl, "%d", n); //dynamische Erstellung des Namens der Datendatei
	strcpy(dataFileName, "data/verteilung"); 
	strcat(dataFileName, zahl);
	strcat(dataFileName, ".data");
	
	FILE *fp_daten;
	fp_daten = fopen(dataFileName, "w");
	
	if(fp_daten == NULL){
		printf("Datei konnte nicht geoeffnet werden.\n");
	}else{
		fprintf(fp_daten, "#%.2f\n", sonneRadius);
		for(int i=0; i<anzahlBin; i++){
			fprintf(fp_daten, "%.30f\t", *(x_achse+i)); //schreibe x-Werte in die Datendatei
			fprintf(fp_daten, "%d\n", *(y_achse+i)); //schriebe y-Werte in die Datendatei
		}
		
		fclose(fp_daten);
	}
}

/*
 * Erstellt die Plotdatei für GNUPLOT
 * Parameter: Zahl der aktuellen Simulation als int,
 * 	Anzahl der zu entkommenden Photonen als int,
 * 	Sonnenradius in Meter als double
 * Return: kein Return
 */
void plotDatei(int n, int durchlaeufe, double sonneRadius){
	char zahl[5];
	char plotFileName[30];
	
	sprintf(zahl, "%d", n); //dynamische Erstellung des Namens für die Plotdatei
	strcpy(plotFileName, "plot/verteilung");
	strcat(plotFileName, zahl);
	strcat(plotFileName, ".plot");
	
	FILE *fp_plot;
	
	fp_plot = fopen(plotFileName, "w");
	
	if(fp_plot == NULL){
		printf("Datei konnte nicht geoeffnet werden.\n");
	}else{
		fprintf(fp_plot, "reset\n");
		fprintf(fp_plot, "set title 'Aufgabe 2 - Simulation %d Photonen, %.2fm Radius';\n", durchlaeufe, sonneRadius);
		fprintf(fp_plot, "set xlabel 't_e_s_c [s]';\nset ylabel 'Anzahl der Photonen';\nset grid;\n");
		fprintf(fp_plot, "set terminal pdf;\nset output 'pdf/verteilung%d.pdf';\n", n);
		fprintf(fp_plot, "plot 'data/verteilung%d.data' title 'Datenpunkte' with boxes;", n);
		
		fclose(fp_plot);
	}
}
