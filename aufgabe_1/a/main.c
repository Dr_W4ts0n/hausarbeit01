#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

/*
 * Hier werden alle verwendeten Funktionen zunächst definiert,
 * um Ordnung zu schaffen und problemlos Funktionen im Code an einer
 * beliebigen Stelle zu deklarieren.
 */
int zieheKarte();
int * ziehePaeckchen();
int befuelleAlbum();
int * befuellVerteilung(int);
void outputDatei(int);

int main(){
    srand(time(NULL));
    printf("Ich rechne...\n");
	outputDatei(10000000);
	system("gnuplot -persist verteilung.plot");
	printf("Fertig!\n");
    return 0;
}
/*
 * Zieht eine zufällige ganze Zahl zwischen 0 und 534, die einer Karte entspricht
 * In der main-Funktion ist der Seed für die rand()-Funktion angegeben. 
 * Return: Ganze Zufallszahl zwischen 0 und 534 (int)
 */
int zieheKarte(){
    return rand() % 535;
}

/*
 * Diese Funktion führt 5 mal die Funktion zieheKarte() aus
 * und speichert die Werte in einem int Array, was einem
 * Päckchen von 5 Karten entspricht.
 * Return: Zeiger für unser Päckchen Array (int *)
 * 
 */
int * ziehePaeckchen(){
	static int paeckchen[5]; //unser Array muss statisch deklariert werden, damit der Zeiger problemlos zurückgegeben werden kann.
	
    for(int i=0; i<5;i++){
        paeckchen[i] = zieheKarte();
    }
    return paeckchen;
    
}

/*
 * Diese Funktion simuliert ein Album, zieht Päckchen, solange dieses nicht voll ist,
 * und klebt die Karten ein.
 * Return: gibt die Anzahl an Päckchen zurück, die benötigt wurde, um
 * ein Album zu füllen (int)
 */
int befuelleAlbum(){
	int *paeckchen; //Zeiger um auf die Päckchen aus der Funktion ziehePäckchen() zuzugreifen.
	
    int countSticker = 0; //Zählt wieviele Karten wir in unser Album eingeklebt haben.
    int countPaeckchen = 0; //Zählt wieviele Päckchen wir benötigt haben (Rückgabe)
    int album[535]; //Entspricht unserem Album. Jedes Element was 0 ist, enthält keine Karte,
					//1 entspricht widerum einer eingeklebten Karte
					//Index entspricht der Zahl der Karte, also unsere Zufallszahl
    
    for(int i=0; i<535;i++){
        album[i] = 0;	//es sind noch keine Karten im Album (zur Sicherheit)
    }
    while(countSticker < 535){  //solange das Album nicht voll ist...
		paeckchen = ziehePaeckchen();   //...werden neue Päckchen gezogen
		countPaeckchen++;   //Und die Anzahl an gekauften Päckchen steigt
		for(int i=0;i<5;i++){
			if(album[*(paeckchen + i)] != 1){   //Es wird kontrolliert ob der i-te Sticker aus dem Päckchen schon eingeklebt ist...
				album[*(paeckchen + i)] = 1;    //...wenn nicht wird dieser eingeklebt
				countSticker++;
			}
		}
		
	}
	return countPaeckchen;
}

/*
 * Führt die Simulationen durch und gibt die Verteilung zurück
 * Parameter: Anzahl an Durchführungen bzw. Alben (int)
 * Return: Zeiger für unser Verteilungs-Array, in dem gespeichert ist, wie oft wie viele Päckchen 
 * für ein Album geöffnet werden mussten. (int*)
 * */
int * befuellVerteilung(int nAlben){
	static int verteilung[2001]; //geht von 0 bis 2000, weil Simulationen gezeigt haben, dass die Werte darin liegen
								//Index entspricht der Anzahl an benötigten Päckchen und der jeweilge Wert entspricht
								//der Durchgänge, die ebendiese Päckchenanzahl benötigt haben.
	int temp = 0; //temporäre Hilfvariable für die Päckchenanzahl
	
	for(int i=0;i<2001;i++){
		verteilung[i] = 0;
	}
	
	for(int i=0;i<nAlben;i++){    //1000000 Alben werden befüllt
		temp = befuelleAlbum();     //temp=wie viele Päckchen benötigt wurden um das Album voll zu kriegen
		if(temp <2001){
			verteilung[temp]++;     
		}
    }	
	return verteilung;
}
/*
 * Schreibt unsere Daten in eine Datei für GNUPLOT
 * Parameter: Anzahl an Durchführungen bzw. Alben (int)
 * Return: kein return
 */
void outputDatei(int nAlben){
	int *verteilung;	//hier wird unsere Verteilung übernommen
	verteilung = befuellVerteilung(nAlben);
	
	FILE *fp_daten;
	
	fp_daten = fopen("verteilung.data", "w");
	
	if(fp_daten == NULL){
		printf("Datei konnte nicht geoeffnet werden.\n");
	}else{
		for(int i=307;i<2001;i++){
			fprintf(fp_daten, "%d\t", i); //Werte der x-Achse werden in die Datei reingeschrieben
			fprintf(fp_daten, "%d\n", verteilung[i]); //Werte der y-Achse werden in die Datei reingeschrieben
		}
		
		fclose(fp_daten);
	}
	
}
