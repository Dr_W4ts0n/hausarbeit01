reset
set title "Aufgabe 1 a";
set xlabel "# Päckchen";
set ylabel "# Durchführungen";
set grid;
set terminal pdf;
set output "verteilung.pdf";

plot "verteilung.data" title "Datenpunkte" with boxes;
